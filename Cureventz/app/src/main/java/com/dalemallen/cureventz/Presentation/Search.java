package com.dalemallen.cureventz.Presentation;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.dalemallen.cureventz.R;
import com.dalemallen.cureventz.Business.Event;
import com.dalemallen.cureventz.Data.EventAdapter;
import com.dalemallen.cureventz.Data.EventDatabase;

import java.util.ArrayList;
import java.util.List;

public class Search extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    Intent intent;

    private List<Event> eventList = new ArrayList<>();
    private RecyclerView recyclerView;
    private EventAdapter mAdapter;

    private SharedPreferences sharedPreferences;

    BottomNavigationItemView create;
    BottomNavigationItemView change;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        bottomNavigationView = findViewById(R.id.navigation);

        create = findViewById(R.id.menu_create);
        change = findViewById(R.id.menu_user_mod);
        create.setVisibility(View.GONE);
        change.setVisibility(View.GONE);

        sharedPreferences = getSharedPreferences("userPref", Activity.MODE_PRIVATE);
        final String userType = sharedPreferences.getString("userType", "");

        bottomNavigationView.setSelectedItemId(R.id.menu_search);

        if (userType.equals("2")) {
            create.setVisibility(View.VISIBLE);
        }
        if (userType.equals("3")) {
            create.setVisibility(View.VISIBLE);
            change.setVisibility(View.VISIBLE);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.menu_search) {
                    intent = new Intent(getApplicationContext(), Search.class);
                    startActivity(intent);
                } else if (id == R.id.menu_profile) {
                    intent = new Intent(getApplicationContext(), userView.class);
                    startActivity(intent);
                } else if (id == R.id.menu_list) {
                    intent = new Intent(getApplicationContext(), MyList.class);
                    startActivity(intent);
                } else if (id == R.id.menu_create) {
                    intent = new Intent(getApplicationContext(), CreateEvent.class);
                    startActivity(intent);
                } else if (id == R.id.menu_user_mod) {
                    intent = new Intent(getApplicationContext(), AdminPanel.class);
                    startActivity(intent);
                }

                return true;
            }
        });

        //build Room object.
        EventDatabase dbev = Room.databaseBuilder(getApplicationContext(), EventDatabase.class, "events").allowMainThreadQueries().build();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_event);
        mAdapter = new EventAdapter(eventList, true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        //get current users from Room db.
        ArrayList<Event> temp = (ArrayList<Event>) dbev.eventDao().getAllEvents();

        //cycle threw all users add them to TextView and userList.
        for(Event event: temp) {

            Log.d("Found event"," "+event);
            eventList.add(event);
            //users.setText(user.getUserName());

        }

        dbev.close();

    }
}
