package com.dalemallen.cureventz.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import com.dalemallen.cureventz.Business.Event;

/*
 * Event database dao page.
 * @author Gideon Landry
 * @since Febuary 12,2017
 */

@Dao
public interface EventDao {

    @Query("SELECT * FROM event")
    List<Event> getAllEvents();

    @Insert
    void insertAll(Event... events);

    @Update
    void updateAll(Event... events);

    @Delete
    void deleteOne(Event event);

}
