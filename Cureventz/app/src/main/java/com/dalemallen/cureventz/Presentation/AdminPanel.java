package com.dalemallen.cureventz.Presentation;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dalemallen.cureventz.R;
import com.dalemallen.cureventz.Business.User;
import com.dalemallen.cureventz.Data.UserDatabase;

import java.util.ArrayList;
import java.util.List;

public class AdminPanel extends AppCompatActivity {

    EditText editText;
    RadioGroup radioGroup;

    private SharedPreferences sharedPreferences;
    BottomNavigationView bottomNavigationView;
    BottomNavigationItemView create;
    BottomNavigationItemView change;

    User user;
    Button button;
    CheckBox checkBox;
    private final List<User> userList = new ArrayList<>();

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panel);

        bottomNavigationView = findViewById(R.id.navigation);

        sharedPreferences = getSharedPreferences("userPref", Activity.MODE_PRIVATE);
        final String userType = sharedPreferences.getString("userType", "");

        create = findViewById(R.id.menu_create);
        change = findViewById(R.id.menu_user_mod);
        create.setVisibility(View.GONE);
        change.setVisibility(View.GONE);



        bottomNavigationView.setSelectedItemId(R.id.menu_user_mod);

        //bottomNavigationView.setSelectedItemId();
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();

                if (id == R.id.menu_search) {
                    intent = new Intent(getApplicationContext(), Search.class);
                    startActivity(intent);
                } else if (id == R.id.menu_profile) {
                    intent = new Intent(getApplicationContext(), userView.class);
                    startActivity(intent);
                }
                if (id == R.id.menu_list) {
                    intent = new Intent(getApplicationContext(), MyList.class);
                    startActivity(intent);
                }
                if (id == R.id.menu_create) {
                    intent = new Intent(getApplicationContext(), CreateEvent.class);
                    startActivity(intent);
                }
                if (id == R.id.menu_user_mod) {
                    intent = new Intent(getApplicationContext(), AdminPanel.class);
                    startActivity(intent);
                }
                return true;
            }
        });

        if (userType.equals("2")) {
            create.setVisibility(View.VISIBLE);
        }
        if (userType.equals("3")) {
            create.setVisibility(View.VISIBLE);
            change.setVisibility(View.VISIBLE);
        }

        //  db
        final UserDatabase db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "users").allowMainThreadQueries().build();

        // add user to list
        for (int i = 0; i < db.userDao().getAllUsers().size(); i++) {
            userList.add(db.userDao().getAllUsers().get(i));
        }

        editText = findViewById(R.id.editTextName);
        String username = editText.getText().toString();

        for (int i = 0; i < userList.size(); i++) {
            if (username.equals(userList.get(i).getUserName())) {
                Toast.makeText(getApplicationContext(), "The user " + username, Toast.LENGTH_SHORT);
                // matches the user to the username
                user = userList.get(i);
            }
        }

        radioGroup = findViewById(R.id.radioGroup2);
        // change userType
        radioGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Is the button now checked?
                boolean checked = ((RadioButton) view).isChecked();

                // Check which radio button was clicked
                switch (view.getId()) {
                    case R.id.radioButtonBasic:
                        if (checked)
                            // Show userType 1 only
                            user.setUserType("1");
                        db.userDao().updateUser(user);
                        Toast.makeText(getApplicationContext(),user.toString(),Toast.LENGTH_SHORT);

                        break;
                    case R.id.radioButtonPromoter:
                        if (checked)
                            // Show userType 2 only
                            user.setUserType("2");
                        db.userDao().updateUser(user);

                        break;
                }
            }
        });

        checkBox = findViewById(R.id.checkBox);
        // if checked delete user

        button = findViewById(R.id.buttonSubmit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox.isChecked()) {
                    db.userDao().deleteOne(user);

                }

            }

        });

        db.close();
    }
}
