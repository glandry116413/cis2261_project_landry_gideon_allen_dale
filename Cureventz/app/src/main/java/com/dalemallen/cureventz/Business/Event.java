package com.dalemallen.cureventz.Business;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by glandry116413 on 2/7/2018.
 */

@Entity
public class Event {

    @PrimaryKey(autoGenerate = true)
    private int eventID;

    @ColumnInfo(name = "userID")
    private String userID;

    @ColumnInfo(name = "eventAttending")
    private int eventAttending;

    @ColumnInfo(name = "eventType")
    private String eventType;

    @ColumnInfo(name = "eventDescription")
    private String eventDescription;

    @ColumnInfo(name = "eventImg")
    private String eventImg;

    @ColumnInfo(name = "eventTitle")
    private String eventTitle;

    @ColumnInfo(name = "eventLocation")
    private String eventLocation;

    @ColumnInfo(name = "eventTime")
    private String eventTime;

    @ColumnInfo(name = "eventDate")
    private String eventDate;

    @ColumnInfo(name = "createdDate")
    private String createdDate;

    public Event() {
    }

    @Ignore
    public Event(int eventID) {
        this.eventID = eventID;
    }

    @Ignore
    public Event(String userID, String eventTitle, String eventDescription) {

        this.userID = userID;
        this.eventTitle = eventTitle;
        this.eventDescription = eventDescription;

    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getEventAttending() {
        return eventAttending;
    }

    public void setEventAttending(int eventAttending) {
        this.eventAttending = eventAttending;
    }


    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventImg() {
        return eventImg;
    }

    public void setEventImg(String eventImg) {
        this.eventImg = eventImg;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }


    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    @Override
    public String toString() {

        //need a online db. This is currently a place holder.
        return "Event: " + eventID + " Location: " + eventLocation;
    }

}
