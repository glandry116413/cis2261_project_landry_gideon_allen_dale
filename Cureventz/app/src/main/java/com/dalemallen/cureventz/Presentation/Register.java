package com.dalemallen.cureventz.Presentation;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dalemallen.cureventz.R;
import com.dalemallen.cureventz.Business.User;
import com.dalemallen.cureventz.Data.UserDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Register extends AppCompatActivity {
    //variables
    private User newUser;
    private List<User> userList = new ArrayList<>();

    EditText username;
    EditText email;
    EditText pass;
    EditText rePass;
    TextView textViewLogin;
    Button create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = findViewById(R.id.editTextUsername);
        email = findViewById(R.id.editTextEmail);
        pass = findViewById(R.id.editTextPassword);
        rePass = findViewById(R.id.editTextRepassword);
        create = findViewById(R.id.buttonCreate);
        //build Room object.

        try {

            final UserDatabase db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "users").allowMainThreadQueries().build();

            newUser = new User("3", "adminD", "dale@gmail.com", "", "1234");
            // add admin to db
            userList.add(newUser);
            db.userDao().insertAll(newUser);
            newUser = new User("3", "adminG", "gideon@gmail.com", "", "1234");
            // add admin to db
            userList.add(newUser);
            db.userDao().insertAll(newUser);
            newUser = new User("2", "testUser", "test@gmail.com", "", "test");
            // add promo to db
            userList.add(newUser);
            db.userDao().insertAll(newUser);
            // load all users into userList
            userList.addAll(db.userDao().getAllUsers());

            db.close();

        } catch (Exception e) {
            Log.d("Users", "except ", e);
        }

        // click create
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // get both passwords
                String pass1 = pass.getText().toString();
                String pass2 = rePass.getText().toString();

                // if no field is empty
                if (username.getText().toString().length() != 0 && email.getText().toString().length() != 0 && pass.getText().toString().length() != 0 && rePass.getText().toString().length() != 0) {
                    // if both passwords are equal set user information
                    if (pass1.equals(pass2)) {

                        //new user then set fields
                        newUser = new User();
                        newUser.setUserName(username.getText().toString());
                        newUser.setUserEmail(email.getText().toString());
                        newUser.setPass(pass.getText().toString());
                        newUser.setUserType("1");
                        Date c = Calendar.getInstance().getTime();
                        newUser.setDateCreated("" + c);
                        // loop through database total users
                        for (int i = 0; i < userList.size(); i++) {
                            // if username is not already in the db create then add them to db
                            if (!newUser.getUserName().equals(userList.get(i).getUserName())) {

                                try {

                                    final UserDatabase db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "users").allowMainThreadQueries().build();

                                    // add user
                                    db.userDao().insertAll(newUser);
                                    // close db
                                    db.close();
                                    Toast.makeText(getApplication(), "Welcome " + newUser.getUserName(), Toast.LENGTH_SHORT).show();
                                    //push user to login page.
                                    Intent intent = new Intent(getApplicationContext(), Login.class);
                                    startActivity(intent);

                                } catch (Exception e) {
                                    Log.d("except", "Error " + e);
                                }

                            } else {
                                Toast.makeText(getApplication(), "User already exist!", Toast.LENGTH_SHORT).show();

                            }
                        }
                    } else {
                        Toast.makeText(getApplication(), "Password doesn't Match", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplication(), "A Field is empty", Toast.LENGTH_SHORT).show();
                }
            }

        });

        textViewLogin = findViewById(R.id.textViewLogin);
        textViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });
    }
}
