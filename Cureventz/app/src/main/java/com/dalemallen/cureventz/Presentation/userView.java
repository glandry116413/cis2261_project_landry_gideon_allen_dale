package com.dalemallen.cureventz.Presentation;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.dalemallen.cureventz.Data.UserAdapter;
import com.dalemallen.cureventz.Data.UserDatabase;
import com.dalemallen.cureventz.Business.User;
import com.dalemallen.cureventz.R;

import java.util.ArrayList;
import java.util.List;

public class userView extends AppCompatActivity {

    private List<User> userList = new ArrayList<>();
    private RecyclerView recyclerView;
    private UserAdapter mAdapter;
    private SharedPreferences sharedPreferences;
    BottomNavigationView bottomNavigationView;
    BottomNavigationItemView create;
    BottomNavigationItemView change;
    Button button;
    Boolean islogged;
    Intent intent;

    //TextView users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);

        bottomNavigationView = findViewById(R.id.navigation);
        //users = findViewById(R.id.users);

        create = findViewById(R.id.menu_create);
        change = findViewById(R.id.menu_user_mod);
        create.setVisibility(View.GONE);
        change.setVisibility(View.GONE);

        sharedPreferences = getSharedPreferences("userPref", Activity.MODE_PRIVATE);
        final String userType = sharedPreferences.getString("userType", "");
        final String userId = sharedPreferences.getString("userId", "");
        islogged = Boolean.valueOf(sharedPreferences.getString("islogged", "")) ;

        bottomNavigationView.setSelectedItemId(R.id.menu_profile);

        if (userType.equals("2")) {
            create.setVisibility(View.VISIBLE);
        }
        if (userType.equals("3")) {
            create.setVisibility(View.VISIBLE);
            change.setVisibility(View.VISIBLE);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.menu_search) {
                    intent = new Intent(getApplicationContext(), Search.class);
                    startActivity(intent);
                } else if (id == R.id.menu_profile) {
                    intent = new Intent(getApplicationContext(), userView.class);
                    startActivity(intent);
                } else if (id == R.id.menu_list) {
                    intent = new Intent(getApplicationContext(), MyList.class);
                    startActivity(intent);
                } else if (id == R.id.menu_create) {
                    intent = new Intent(getApplicationContext(), CreateEvent.class);
                    startActivity(intent);
                } else if (id == R.id.menu_user_mod) {
                    intent = new Intent(getApplicationContext(), AdminPanel.class);
                    startActivity(intent);
                }

                return true;
            }
        });

        //build Room object.
        final UserDatabase db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "users")
                .allowMainThreadQueries()
                .build();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new UserAdapter(userList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        //get current users from Room db.
        ArrayList<User> temp = (ArrayList<User>) db.userDao().getAllUsersByUserId(Integer.valueOf(userId));

        db.close();

        //cycle threw all users add them to TextView and userList.
        for(User user: temp) {

            Log.d("Found user"," "+user);
            userList.add(user);
            //users.setText(user.getUserName());

        }
        button = findViewById(R.id.buttonLogOut);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences("userPref", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("islogged", "false");
                editor.apply();

                islogged = false;


                if(sp.getString("islogged","").equals("false")) {
                    Intent intent = new Intent(getApplicationContext(), Login.class);
                    startActivity(intent);
                }
            }
        });


        //users.setText(Integer.toString(mAdapter.getItemCount()));

    }
}
