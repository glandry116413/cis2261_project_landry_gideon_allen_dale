package com.dalemallen.cureventz.Presentation;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dalemallen.cureventz.R;
import com.dalemallen.cureventz.Business.Event;
import com.dalemallen.cureventz.Data.EventDatabase;

public class CreateEvent extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    Intent intent;

    EditText title;
    EditText description;
    EditText location;
    EditText time;
    EditText date;
    RadioGroup type;
    RadioButton selected;

    BottomNavigationItemView create;
    BottomNavigationItemView change;
    Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        bottomNavigationView = findViewById(R.id.navigation);

        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        location = findViewById(R.id.location);
        time = findViewById(R.id.time);
        date = findViewById(R.id.date);
        type = findViewById(R.id.type);

        add = findViewById(R.id.add);

        //create and user modification menu options.
        create = findViewById(R.id.menu_create);
        change = findViewById(R.id.menu_user_mod);
        create.setVisibility(View.GONE);
        change.setVisibility(View.GONE);

        SharedPreferences sharedPreferences = getSharedPreferences("userPref", Activity.MODE_PRIVATE);
        final String userType = sharedPreferences.getString("userType", "");

        bottomNavigationView.setSelectedItemId(R.id.menu_create);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();

                if (id == R.id.menu_search) {
                    intent = new Intent(getApplicationContext(), Search.class);
                    startActivity(intent);
                }
                if (id == R.id.menu_profile) {
                    intent = new Intent(getApplicationContext(), userView.class);
                    startActivity(intent);
                }
                if (id == R.id.menu_list) {
                    intent = new Intent(getApplicationContext(), MyList.class);
                    startActivity(intent);
                }
                if (id == R.id.menu_create) {
                    intent = new Intent(getApplicationContext(), CreateEvent.class);
                    startActivity(intent);
                }
                if (id == R.id.menu_user_mod) {
                    intent = new Intent(getApplicationContext(), AdminPanel.class);
                    startActivity(intent);
                }
                return true;
            }
        });

        if (userType.equals("2")) {
            create.setVisibility(View.VISIBLE);
        }
        if (userType.equals("3")) {
            create.setVisibility(View.VISIBLE);
            change.setVisibility(View.VISIBLE);
        }

        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Event event = new Event();

                selected = findViewById(type.getCheckedRadioButtonId());

                event.setCreatedDate("2/13/2018");
                event.setEventAttending(0);
                event.setEventDate(date.getText().toString());
                event.setEventDescription(description.getText().toString());
                event.setEventImg("none");
                event.setEventLocation(location.getText().toString());
                event.setEventTime(time.getText().toString());
                event.setEventTitle(title.getText().toString());
                event.setEventType(selected.getText().toString());
                event.setUserID(userType);

                EventDatabase db = Room.databaseBuilder(getApplicationContext(), EventDatabase.class, "events").allowMainThreadQueries().build();

                db.eventDao().insertAll(event);

                db.close();

                Toast.makeText(getApplication(),"Added event successfully",Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getApplicationContext(), MyList.class);
                startActivity(intent);

            }

        });


    }
}
