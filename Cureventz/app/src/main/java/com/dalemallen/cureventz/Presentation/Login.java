package com.dalemallen.cureventz.Presentation;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dalemallen.cureventz.R;
import com.dalemallen.cureventz.Business.Event;
import com.dalemallen.cureventz.Data.EventDatabase;
import com.dalemallen.cureventz.Business.User;
import com.dalemallen.cureventz.Data.UserDatabase;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Login extends AppCompatActivity {

    EditText editTextUsername;
    EditText editTextPassword;
    Button btnLogin;
    TextView textViewReg;
    TextView textViewUsername;
    CallbackManager callbackManager;
    LoginButton loginButton;

    private SharedPreferences sharedPreferences;
    private User newUser;
    private List<User> userList = new ArrayList<>();
    private final List<Event> eventList = new ArrayList<>();

    Boolean islogged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // get the shared preferences
        sharedPreferences = getSharedPreferences("userPref", Activity.MODE_PRIVATE);
        // set the string pref to boolean and update islogged
        islogged = Boolean.valueOf(sharedPreferences.getString("isLogged", ""));
        // forward to main page if logged in
        if (islogged) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }


        try {
            //build Room object.
            final UserDatabase db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "users").allowMainThreadQueries().build();

            if(db.userDao().getAllUsers().isEmpty()) {

                newUser = new User("3", "adminD", "dale@gmail.com", "", "1234");
                // add admin to db
                userList.add(newUser);
                db.userDao().insertAll(newUser);
                newUser = new User("3", "adminG", "gideon@gmail.com", "", "1234");
                // add admin to db
                userList.add(newUser);
                db.userDao().insertAll(newUser);
                newUser = new User("2", "testUser", "test@gmail.com", "", "test");
                // add promo to db
                userList.add(newUser);
                db.userDao().insertAll(newUser);
                // load all users into userList



            }

            userList = db.userDao().getAllUsers();

            db.close();

        } catch (Exception e) {

            Log.d("Db fail", "Failed to connect to database.", e);

        }



        EventDatabase evdb = Room.databaseBuilder(getApplicationContext(), EventDatabase.class, "events").allowMainThreadQueries().build();

        ArrayList<Event> temp = (ArrayList<Event>) evdb.eventDao().getAllEvents();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        if (temp.isEmpty()) {

            Event event = new Event();

            event.setCreatedDate(formattedDate);
            event.setEventAttending(0);
            event.setEventDate("21-FEB-2018");
            event.setEventTime("9:00 PM");
            event.setEventDescription("Party In Charlottetown");
            event.setEventType("Party");
            event.setEventLocation("Charlottetown, PEI");
            event.setEventTitle("Party In Charlottetown");
            event.setUserID("1");

            evdb.eventDao().insertAll(event);

        }

        evdb.close();

        callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {


                        String userId = loginResult.getAccessToken().getUserId();

                        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    displayUserInfo(object);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }


                        });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name, last_name, email, id");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();

                        Toast.makeText(getApplication(), "Facebook Login", Toast.LENGTH_SHORT).show();
                        // set logged in to true
                        islogged = true;
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });


        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        btnLogin = findViewById(R.id.buttonLogin);
        textViewReg = findViewById(R.id.txtRegister);

        // if username and password is not empty do this ...

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextUsername.getText().toString().length() != 0 && editTextPassword.getText().toString().length() != 0) {
                    islogged = false;
                    for (int i = 0; i < userList.size(); i++) {
                        // if user names are equal then
                        if (editTextUsername.getText().toString().equals(userList.get(i).getUserName())) {
                            // then if user pass are equal then logging is true and forward to main page
                            if (editTextPassword.getText().toString().equals(userList.get(i).getPass())) {
                                // set logged in to true
                                islogged = true;
                                newUser = userList.get(i);

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("userId", String.valueOf(newUser.getUserID()));
                                editor.putString("userName", String.valueOf(newUser.getUserName()));
                                editor.putString("userType", String.valueOf(newUser.getUserType()));
                                editor.putString("islogdged", "true");
                                editor.apply();

                                Toast.makeText(getApplicationContext(), "Logged in successfully", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);

                            }
                        }
                    }

                    //if no user with matching name and password is found notify user.
                    if (!islogged) {
                        Toast.makeText(getApplication(), "Username or Password is incorrect", Toast.LENGTH_SHORT).show();
                    }

                    //if any field is empty notify user.
                } else {

                    Toast.makeText(getApplication(), "Username or Password is Empty", Toast.LENGTH_SHORT).show();

                }
            }
        });

        textViewReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Register.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void displayUserInfo(JSONObject object) throws JSONException {
        String first_name, last_name, email, id;
        first_name = object.getString("first_name");
        last_name = object.getString("last_name");
        email = object.getString("email");
        id = object.getString("id");

        textViewUsername = findViewById(R.id.textViewFbName);
        textViewUsername.setText(first_name);

    }
}
