package com.dalemallen.cureventz.Data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.dalemallen.cureventz.Business.User;

/*
 * Event database page.
 * @author Gideon Landry
 * @since Febuary 12,2017
 */

@Database(entities = {User.class}, version = 2, exportSchema = false)
public abstract class UserDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}