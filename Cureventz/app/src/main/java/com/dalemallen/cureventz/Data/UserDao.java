package com.dalemallen.cureventz.Data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import com.dalemallen.cureventz.Business.User;

/*
 * User database dao page.
 * @author Gideon Landry
 * @since Febuary 12,2017
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAllUsers();

    @Query("SELECT * FROM user WHERE userName LIKE :username")
    List<User> getAllUsersByUsername(String username);

    @Query("SELECT * FROM user WHERE userID LIKE :userId")
    List<User> getAllUsersByUserId(int userId);

    @Insert
    void insertAll(User... users);

    @Update
    void updateUser(User... user);

    @Update
    void updateAll(User... users);

    @Delete
    void deleteOne(User user);

}
