package com.dalemallen.cureventz.Data;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dalemallen.cureventz.Business.User;
import com.dalemallen.cureventz.R;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private List<User> userList;



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView email,  username, userId;

        public MyViewHolder(View view) {
            super(view);

            email = (TextView) view.findViewById(R.id.email);
            username = (TextView) view.findViewById(R.id.username);
            userId = (TextView) view.findViewById(R.id.userId);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }


    public UserAdapter(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        User user = userList.get(position);
        holder.email.setText(user.getUserEmail());
        holder.username.setText(user.getUserName());
        holder.userId.setText(String.valueOf(user.getUserID()));

        //************************************************************************
        //Storing the tournament id in a final attribute.  It can then be used
        //in the onClickListener to know which tournament is clicked
        //http://stackoverflow.com/questions/39074272/respond-to-buttons-click-in-recyclerview
        //************************************************************************

//        final String userId = user.getUserID();
//        holder.firstName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
//            }
//        });
//        holder.username.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
//            }
//        });
//        holder.userId.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}
