package com.dalemallen.cureventz.Presentation;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dalemallen.cureventz.R;
import com.dalemallen.cureventz.Business.User;
import com.facebook.CallbackManager;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

//    LinearLayout search;
//    LinearLayout list;
//    LinearLayout profile;
    CallbackManager callbackManager;
//    BottomNavigationView bottomNavigationView;
//
//    BottomNavigationItemView create;
//    BottomNavigationItemView change;
//
//    private SharedPreferences sharedPreferences;
//    private final List<User> userList = new ArrayList<>();
//    private User newUser;
//
//    Intent intent;
//    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        create = findViewById(R.id.menu_create);
//        change = findViewById(R.id.menu_user_mod);
//        create.setVisibility(View.INVISIBLE);
//        change.setVisibility(View.INVISIBLE);
//
//        // get shared
//        sharedPreferences = getSharedPreferences("userPref", Activity.MODE_PRIVATE);
//        // get userType and Name
//        final String userType = sharedPreferences.getString("userType", "");
//        String userName = sharedPreferences.getString("userName", "");
//        textView = findViewById(R.id.textViewUser);
//        textView.setText(userName);

        // Set bottom nav actions
//        bottomNavigationView = findViewById(R.id.navigation);
//        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                int id = item.getItemId();
//
//                if (id == R.id.menu_search) {
//                    intent = new Intent(getApplicationContext(), Search.class);
//                    startActivity(intent);
//                } else if (id == R.id.menu_profile) {
//                    intent = new Intent(getApplicationContext(), userView.class);
//                    startActivity(intent);
//                } else if (id == R.id.menu_list) {
//                    intent = new Intent(getApplicationContext(), MyList.class);
//                    startActivity(intent);
//                } else if (id == R.id.menu_create) {
//                    intent = new Intent(getApplicationContext(), CreateEvent.class);
//                    startActivity(intent);
//                } else if (id == R.id.menu_user_mod) {
//                    intent = new Intent(getApplicationContext(), AdminPanel.class);
//                    startActivity(intent);
//                }
//
//                // if userType is 2 show create events
//                if (userType.equals("2")) {
//                    create.setVisibility(View.VISIBLE);
//                }
//                // if userType 3 show create events and admin panel
//                if (userType.equals("3")) {
//                    create.setVisibility(View.VISIBLE);
//                    change.setVisibility(View.VISIBLE);
//                }
//
//                return true;
//            }
//        });

        Intent intent = new Intent(getApplicationContext(), Search.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
