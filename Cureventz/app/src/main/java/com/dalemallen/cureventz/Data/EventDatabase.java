package com.dalemallen.cureventz.Data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.dalemallen.cureventz.Business.Event;

/*
 * Event database page.
 * @author Gideon Landry
 * @since Febuary 12,2017
 */

@Database(entities = {Event.class}, version = 2, exportSchema = false)

public abstract class EventDatabase extends RoomDatabase {
    public abstract EventDao eventDao();

}