package com.dalemallen.cureventz.Data;

import android.arch.persistence.room.Room;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dalemallen.cureventz.Business.Event;
import com.dalemallen.cureventz.R;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {

    private List<Event> eventList;

    private Boolean addButton;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView eventTitle, eventDescription, eventLocation, eventDate, eventTime, eventType;

        public Button btnAddEvent;

        public MyViewHolder(View view) {
            super(view);

            eventTitle = (TextView) view.findViewById(R.id.eventTitle);
            eventDescription = (TextView) view.findViewById(R.id.eventDescription);
            eventLocation = (TextView) view.findViewById(R.id.eventLocation);
            eventDate = (TextView) view.findViewById(R.id.eventDate);
            eventTime = (TextView) view.findViewById(R.id.eventTime);
            eventType = (TextView) view.findViewById(R.id.eventType);

            btnAddEvent = (Button) view.findViewById(R.id.btnAddEvent);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }


    public EventAdapter(List<Event> eventList, Boolean addButton) {

        this.eventList = eventList;
        this.addButton = addButton;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Event event = eventList.get(position);
        holder.eventTitle.append("  " + event.getEventTitle());
        holder.eventDescription.append("  " + event.getEventDescription());
        holder.eventLocation.append("  " + event.getEventLocation());
        holder.eventDate.append("  " + event.getEventDate());
        holder.eventTime.append("  " + event.getEventTime());
        holder.eventType.append("  " + event.getEventType());

        if(addButton){
            holder.btnAddEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        EventDatabase ev = Room.databaseBuilder(v.getContext(), EventDatabase.class, "list").allowMainThreadQueries().build();

                        ev.eventDao().insertAll(event);

                        Toast.makeText(v.getContext(), "Added event.", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {

                        Toast.makeText(v.getContext(), "Failed to add event.", Toast.LENGTH_SHORT).show();

                    }
                }

            });
        } else {

            holder.btnAddEvent.setText("Remove");
            holder.btnAddEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        EventDatabase ev = Room.databaseBuilder(v.getContext(), EventDatabase.class, "list").allowMainThreadQueries().build();

                        ev.eventDao().deleteOne(event);

                        Toast.makeText(v.getContext(), "Removed event.", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {

                        Toast.makeText(v.getContext(), "Failed to remove event.", Toast.LENGTH_SHORT).show();

                    }
                }

            });

        }
        //************************************************************************
        //Storing the tournament id in a final attribute.  It can then be used
        //in the onClickListener to know which tournament is clicked
        //http://stackoverflow.com/questions/39074272/respond-to-buttons-click-in-recyclerview
        //************************************************************************

//        final String userId = user.getUserID();
//        holder.firstName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
//            }
//        });
//        holder.username.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
//            }
//        });
//        holder.userId.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }
}
