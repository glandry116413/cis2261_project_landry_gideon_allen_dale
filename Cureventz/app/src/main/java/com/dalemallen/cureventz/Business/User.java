package com.dalemallen.cureventz.Business;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by glandry116413 on 2/7/2018.
 */

@Entity
public class User {

    public User( String userType, String userName, String userEmail, String dateCreated, String pass) {
        this.userID = getUserID();
        this.userType = userType;
        this.userName = userName;
        this.userEmail = userEmail;
        this.dateCreated = dateCreated;
        this.pass = pass;
    }

    @PrimaryKey(autoGenerate = true)
    int userID;

    @ColumnInfo(name = "userType")
    String userType = "1";

    @ColumnInfo(name = "userName")
    String userName;

    @ColumnInfo(name = "userEmail")
    String userEmail;

    @ColumnInfo(name = "dateCreated")
    String dateCreated;

    @ColumnInfo(name = "pass")
    String pass;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }


    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public User() {


    }

    @Override
    public String toString() {

        return userName;

    }

}
