# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository holds the files for the Cureventz android app for CIS-2261. (Located in Cureventz folder)

There is also beginning files for a web site, but these are currently non functional. (Ignore webPage directory)

### How do I get set up? ###

When loading the app for the first time 2 admin accounts are automatically created:

1. username: adminD  Password: 1234

2. username: adminG  Password: 1234

Use the above accounts at login to have access to all functions of the app.

Register a new account to see only the options avalible to standard users. (note: after creating an account you will still have to login with that account to use the app)
