
drop DATABASE IF EXISTS final_project;
create DATABASE final_project;
use final_project;

/*allow user control of this database*/
grant select, insert, update, delete on final_project.*
to 'TeamL'@'localhost'
identified by 'GK3672EFI9F4';
flush privileges;

CREATE TABLE IF NOT EXISTS `users` (
  `userID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `userName` VARCHAR(30) NOT NULL,
  `userEmail` VARCHAR(30) NOT NULL,
  `userType` INT DEFAULT 0,
  `dateCreated` VARCHAR(20) NOT NULL,
  `pass` VARCHAR(32) NOT NULL
);

CREATE TABLE IF NOT EXISTS `userTypes` (
  `userType` INT NOT NULL,
  `typeName` VARCHAR(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS `events` (
  `eventID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `userID` INT NOT NULL,
  `eventType` VARCHAR(20) NOT NULL,
  `eventDescription` VARCHAR(50),
  `eventImg` VARCHAR(100),
  `eventAttending` INT DEFAULT 0,
  `eventTitle` VARCHAR(20),
  `eventLocation` VARCHAR(100),
  `eventTime` VARCHAR(5),
  `eventDate` DATE,
  `createdDate` DATE
);

CREATE TABLE IF NOT EXISTS `list` (
  `listID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `eventID` TEXT NOT NULL,
  `userid` INT NOT NULL,
  `dateAdded` DATE NOT NULL
);

INSERT INTO `users` VALUES
(1, 'Dale Allen', 'dallen117442@hollandcollege.com', 2, CURRENT_DATE, md5('somethingeasy')),
(2, 'Gideon Landry', 'glandry116413@hollandcollege.com', 2, CURRENT_DATE, md5('creat1ve'));
INSERT INTO `userTypes` VALUES (0, 'Standard'), (1, 'Promoter'), (2, 'Admin');