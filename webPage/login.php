<?php
/**
 * This page will allow users to login and provide a link to create new user.
 * User: Gideon Landry
 * Date: Oct 11, 2017
 */

namespace finalProject;
require 'head.php';

//if user is already logged in send to main page.
if(isset($_SESSION['user'])) {

    header('Location: index.php');
    die();

}
?>

    <title>Login</title>

    <!--  JavaScript  -->
    <link href="encrypt.js"/>

<?php

$login = "active";
require 'bodyHead.php';

?>

    <!-- the div below will hold the entire form, styled by bootstrap -->
    <br><br><br  /><div class="panel panel-primary">

        <!-- this div holds the form title(heading) -->
        <div class="panel-heading">

            Login

        </div>

        <!-- this div holds body of form -->
        <div class="panel-body">
            <form name="send" action="login.php" method="post">
                <div class="form-group">
                    <label for="user">Username</label>
                    <input type="text" class="form-control" id="user" placeholder="Username">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password">
                </div>
                <?php

                    if(isset($_POST['private']) && $_POST['private'] != "") {

                        include_once 'Users.php';

                        $user = new LogedInUser($_POST['private']);

                        if($user->isFound() && $user->isValid()) {

                            $_SESSION['user'] = serialize($user);

                            header('Location: index.php');
                            die();

                        }
                    }

                ?>

                <input type="hidden" name="private" id="private" value="">
                <input type="button" value="Login" class="btn btn-primary" onclick='encrypt()'>
                <a href="createUser.php">Create Account</a>
            </form>
        </div>
    </div>
<?php require 'foot.php'; ?>
