<?php
/**
 * This page will allow a user to input their information to create a new user.
 * User: Gideon Landry
 * Date: oct 7, 2017
 */


//if user is already logged in send to main page.
if(isset($_SESSION['user'])) {

    header('Location: index.php');
    die();

}

require 'head.php';
?>

        <title>New Account</title>

    <?php

    $createUser = "active";
    require 'bodyHead.php';
    //if private field exists and is not an empty string begin CAPTCHA validation.
    if(isset($_POST['private']) && $_POST['private'] != "") {

        //CAPTCHA validation code from https://www.phpcaptcha.org/documentation/quickstart-guide/
        //the securimage.php directory changes based on project location
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . 'webPage/securimage/securimage.php')) {

            include_once $_SERVER['DOCUMENT_ROOT'] . 'webPage/securimage/securimage.php';

        } elseif(file_exists($_SERVER['DOCUMENT_ROOT'] . '/Main2/cis2261_project_landry_gideon_allen_dale/webPage/securimage/securimage.php')) {

            include_once $_SERVER['DOCUMENT_ROOT'] . '/Main2/cis2261_project_landry_gideon_allen_dale/webPage/securimage/securimage.php';

        } else {

            include_once $_SERVER['DOCUMENT_ROOT'] . /*Gideon: The following directory may need to be changed depending on where you place this project.*/ '/securimage/securimage.php';

        }
        $securimage = new Securimage();

        if ($securimage->check($_POST['captcha_code']) == false) {

            echo "The security code entered was incorrect.<br /><br />";
            echo "Please go <a href='javascript:history.go(-1)'>back</a> and try again.";
            exit;

        //if CAPTCHA is correct create NewUser object.
        } else {

            include_once 'Users.php';

            $newUser = new \finalProject\NewUser($_POST['private']);

            if(!($newUser->isFound())) {

                //stores User Object so it can properly be used later. (puts object to sleep)
                $_SESSION['user'] = serialize($newUser);


                //notifies user that account was created successfully and sends them to index.php after 3.5 seconds, displays loading icon during wait.
                echo "<h1 class='success'>Success!!!!</h1><br><h4>Account created successfully, redirecting you to main page.</h4>
                    <div class='load'></div>
                    <script>
                        setTimeout(function() {
                            window.location.assign('index.php');
                        }, 3500);
                    </script></body></html>";
                die();

            } else {

                echo "<h3 style='warning'>User already exists</h3>";

            }


        }



    }
    ?>

    <!-- the div below will hold the entire form, styled by bootstrap -->
        <br><br><br  /><div class="panel panel-primary">

                <!-- this div holds the form title(heading) -->
                <div class="panel-heading">

                    Account Creation

                </div>

                <!-- this div holds body of form -->
                <div class="panel-body">
                    <form name="send" action="createUser.php" method="post">
                        <div class="form-group">
                            <label for="user">Username*</label>
                            <input type="text" class="form-control" id="user" placeholder="Username">
                        </div>

                        <!-- password input -->
                        <div class="form-group">
                            <label for="password">Password*</label>
                            <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>

                        <!-- confirm password. passwords are compared in by javascript in encrypt.js -->
                        <div class="form-group">
                            <label for="confpassword">Confirm Password*</label>
                            <input type="password" class="form-control" id="confpassword" placeholder="Confirm Password">
                        </div>

                        <!-- CAPTCHA to make sure user is a real person. -->
                        <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image" />
                        <div class="form-group">
                            <input type="text" name="captcha_code" size="10" maxlength="6" />
                            <a href="#" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false">[ Different Image ]</a>
                        </div>

                        <input type="hidden" name="private" id="private" value="">

                        <div class="form-group">

                            <!-- Instead of submitting the form this button calls the encryptAll javascript function -->
                            <input type="button" value="Create" class="btn btn-primary" onclick='encrypt()'>

                            <h4 class="warning" id="passVal">Passwords do not match!</h4>
                        </div>
                        <p>Already have an account? <a href="login.php">Login.</a></p>
                        <p class="required">*required</p>
                    </form>
                </div>
        </div>
<?php require 'foot.php'; ?>

