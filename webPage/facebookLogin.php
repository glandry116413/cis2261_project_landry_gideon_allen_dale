<?php
/**
 * Facebook login page containing any code required to connect to facebook.
 * User: Gideon Landry
 * Date: January 29, 2018
 */

//includes head tag content excluding title.
require 'head.php';

?>
    <title>Facebook Login</title>

<!-- closes head and starts body of page, includes nav bar. -->
<?php

    require 'bodyHead.php';

    $fb = new Facebook\Facebook([
        'app_id' => '{app-id}', // Replace {app-id} with your app id
        'app_secret' => '{app-secret}',
        'default_graph_version' => 'v2.2',
    ]);

    $helper = $fb->getRedirectLoginHelper();

    $permissions = ['email']; // Optional permissions
    $loginUrl = $helper->getLoginUrl('https://example.com/fb-callback.php', $permissions);

    echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';

?>


<!-- BODY CONTENT GOES HERE! -->


<!-- closes body (thus page), will include any footer content. -->
<?php require 'foot.php'; ?>
