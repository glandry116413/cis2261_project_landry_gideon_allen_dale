<?php
/**
 * Personal project header
 * User: Gideon Landry
 * Created: November 15, 2017
 * Modified: November 15, 2017
 */
namespace finalProject;
session_start();
$index = "";
$login = "";
$profile = "";
$createUser = "";


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--  My personal CSS  -->
    <link rel="stylesheet" href="projectStyles.css"/>


    <!--Gideon: the following link and scripts allow bootstrap styling on this page. -->
    <!-- CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
