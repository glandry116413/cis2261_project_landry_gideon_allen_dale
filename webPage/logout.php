<?php
/**
 * This page will log a user out (end session).
 * User: Gideon Landry
 * Date: October 16, 2017
 */

session_start();
session_destroy();
header('Location: index.php');
die();