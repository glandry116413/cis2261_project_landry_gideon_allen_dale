/**
 * This function page is used to validate and encrypt user information.
 * Author: Gideon Landry
 * Created: September 4, 2017
 * Modified: November 15, 2017
 */

function encrypt() {

    var user = document.getElementById('user').value;
    var pass = document.getElementById('password').value;

    //checks if this function is being called from creatUser.php or login.php.
    if(document.getElementById('confpassword')) {

        //compares passwords.
        if(document.getElementById('confpassword').value !== document.getElementById('password').value) {

            document.getElementById('passVal').style.visibility = "visible";


        } else {

            document.getElementById('passVal').style.visibility = "hidden";
            var carrier = user + ":" + pass;

            //encrypts data.
            carrier = encodeURIComponent(window.btoa(carrier));

            //places encrypted date in form.
            document.getElementById('private').value = carrier;

            //submits the form.
            document.send.submit();

        }

    } else {

        var carrier = user + ":" + pass;

        carrier = encodeURIComponent(window.btoa(carrier));

        document.getElementById('private').value = carrier;

        document.send.submit();

    }



}
