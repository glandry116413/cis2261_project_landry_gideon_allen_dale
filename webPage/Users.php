<?php
/**
 * This is a standard page of class used in conjunction with a database.
 * User: Gideon Landry
 * Created: September 4, 2017
 * Modified: November 15, 2017
 */

namespace finalProject;

//the parent class for this page.
class Users
{

    private $userID;
    private $userName;
    private $userEmail;
    private $userType;
    private $dateCreated;
    private $pass;
    private $found;
    private $valid;

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @param bool $found
     */
    public function setFound($found)
    {
        $this->found = $found;
    }

    /**
     * @param mixed $userID
     * @return Users
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
        return $this;
    }

    /**
     * @param mixed $userName
     * @return Users
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @param mixed $userEmail
     * @return Users
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @param mixed $pass
     * @return Users
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
        return $this;
    }

    /**
     * @param mixed $userType
     * @return Users
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @return int
     */
    public function getUserType()
    {
        return $this->userType;
    }


    /**
     * @return bool
     */
    public function isFound()
    {
        return $this->found;
    }

    /**
     * The parent class constructor will determine if username exists in database.
     * @param $raw
     * @param bool $newUser
     */
    function __construct($raw, $newUser)
    {

        //decrypts raw input
        $raw = urldecode(base64_decode($raw));

        //separates info held in raw input.
        $up = explode(":","$raw");

        //accesses database
        $mysqli = new \mysqli("localhost","TeamL","GK3672EFI9F4","final_project");

        //if there is an error connecting to database end page load with error message.
        if ( $mysqli->connect_error ) {
            die('Connect error ('. $mysqli->connect_errno .') '
                . $mysqli->connect_error);
        }

        //cleanses user information.
        $this->userName = $mysqli->real_escape_string("$up[0]");
        $this->pass = $mysqli->real_escape_string("$up[1]");

        //builds sql query.
        $query = "SELECT count(*) FROM users WHERE username="."'$this->userName'";

        //searches database for username.
        $result = $mysqli->query($query);


        //if there is an issue with the sql statement end page load with error message.
        if ($mysqli->error) {
            die($mysqli->error);
        }

        //holds the number of results found
        $row = $result->fetch_row();

        //if a single result is found set valid to true else set valid to false.
        if ( $row[0] == 1 ) {

            $this->found = true;

        } else {

            $this->found = false;

        }

        if($newUser && !$this->found) {

            $this->newUser();

        } elseif($this->found) {

            $this->login();

        }
        //closes database connection.
        $mysqli->close();

    }

    //this function will create a new user.
    function newUser() {

        if(!$this->isFound()) {

            $mysqli = new \mysqli("localhost","TeamL","GK3672EFI9F4","final_project");

            if ( $mysqli->connect_error ) {
                die('Connect error ('. $mysqli->connect_errno .') '
                    . $mysqli->connect_error);
            }

            $userName = $this->getUserName();
            $password = $this->getPass();

            $query = $mysqli->query("INSERT INTO users (userName, pass) VALUES ('$userName', md5('$password'))");

            if(!$query) {

                echo "<h3 class='warning'>ERROR!</h3>";

            }

        }

    }

    //this function will log a user in.
    function login() {

        $mysqli = new \mysqli("localhost","TeamL","GK3672EFI9F4","final_project");

        $userName = $this->getUserName();
        $password = $this->getPass();

        $query = "SELECT userid FROM users WHERE username="."'$userName' AND pass=md5('$password')";

        $result = $mysqli->query($query);

        if ($mysqli->error) {
            die($mysqli->error);
        }

        $row = $result->fetch_row();

        //if user is found add attributes
        if(mysqli_num_rows($result) != 0) {

            $this->setUserID($row[0]);
            $id = $this->getUserID();
            echo "$id";
            $this->valid = true;

        //if no user is found notify user that credentials are incorrect.
        } else {

            $this->valid = false;
            echo "<h4 class='warning'>Wrong username or password.</h4>";

        }

    }

}
