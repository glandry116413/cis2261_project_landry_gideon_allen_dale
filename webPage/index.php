<?php
/**
 * Personal project main page
 * User: Gideon Landry
 * Created: November 15, 2017
 * Modified: November 15, 2017
 */

if(!isset($_SESSION['user'])) {

    header('Location: login.php');
    die();

}

require 'head.php';

?>

    <title>Home</title>

<?php

$index = "active";
require 'bodyHead.php';

?>

    <div id="left">

        <h1>Left Content</h1>

    </div>

    <div id="right">

        <h1>Right Content</h1>

    </div>

<?php require 'foot.php'; ?>
