<?php
/**
 * This page will hold a body header (site name, menu-options; user name, type, options)
 * User: Gideon Landry
 * Created: November 15, 2017
 * Modified: November 15, 2017
 */
?>
</head>
<body>

    <!-- This nav bar allows user to switch to any page on site (each page has the same nav bar) -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Cureventz</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="<?php echo $index ?>"><a href="index.php">Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <?php

                //changes nav bar based on if user is logged in or not.
                if(isset($_SESSION['user'])) {

                    include_once 'Users.php';

                    //awakens User object.
                    $user = unserialize($_SESSION['user']);
                    $userName = $user->getUser();
                    echo "<li class=\"$profile\"><a href='profile.php'><span class=\"glyphicon glyphicon-user\">$userName</span></a></li>
                                    <li><a href=\"logout.php\"><span class=\"glyphicon glyphicon-log-out\"></span> Logout</a></li>";

                } else {

                    echo "<li class=\"$createUser\"><a href=\"createUser.php\"><span class=\"glyphicon glyphicon-user\"></span> Sign Up</a></li>
                                    <li class=\"$login\"><a href=\"login.php\"><span class=\"glyphicon glyphicon-log-in\"></span> Login</a></li>";

                }

                ?>
            </ul>
        </div>
    </nav>
    <div class="container">